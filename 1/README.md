# Agenda

## Development/Engineering
- Types of Testing
  - Unit vs Integration vs Acceptance & TDD
  - Basic unit-testing with `unittest`
  - *Assignment*: _Write (purposefully failing!) tests for each problem in the programming p-set. The *primary learning objectives* are project structure, testing edge cases, and reasoning about indices._

- Thoughts on Design(ing Features)
  - Behavior Driven Development (BDD) and Gherkin Syntax
- Introducing Aloe
  - We'll break down the `manifest` feature into a few important steps, and take a look at a BDD-style test for a simple function that creates an empty file called `manifest`.
- Writing Tests for the CLI
  - As an exercise, we'll write feature specs for the rest of the feature, so we have some failing tests to work on.
- *Assignment*: Install `aloe`, and write the remaining failing feature tests for the `manifest` feature.

Once the tests are up and running, the next steps are:
- Passing the tests (obviously)
- Git Hooks & Continuous Integration w/ Travis CI

...Which we'll talk about next time.

## Programming/CS
As for CS stuff, we don't have to touch on any of these today, but was thinking this sequence makes sense (each bullet is a different session, realistically):
- Arrays vs Lists & Operations on Linked Lists
  - *Assignment*: _Implement all operations on a generically-typed `LinkedList` class. Implement both singly- and doubly-linked variants._
- Interlude: Collections
  - Stacks, Queues, and Sets
- Recursive Functions & Lists as a Recursive Data Structure
  - *Assignment*: _Re-implement `LInkedList` as a _recursive_ class._
- Binary Search Trees
  - *Assignment*: _Implement a `BinaryTree` tree class, with a `search` method. As a *bonus*, implement `insert` and `delete`._
- Priority Queues
  - *Assignment*: _Implement a generic `heap` class with `insert` and `pop` methods. This is a *considerably tricky* challenge, but I encourage you to chip away at it until you get it._
- *Graphs*
  - *Assignment*: _Implement a generic `Graph` data type with `bfs` and `dfs` methods._

## Problem Set
...And a programming problem set to send your way. These are from https://leetcode.com, and are from the "easy" problem set. They're not _that_ easy—there are some subtleties here and there—but they're mainly good practice for writing unit tests, which is why I call them out.
- [Remove Element](https://leetcode.com/problems/remove-element/)
- [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/) — _This one is important_
- [Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/)
- [strStr](https://leetcode.com/problems/implement-strstr/)
- [Search Insert Position](https://leetcode.com/problems/search-insert-position/)
