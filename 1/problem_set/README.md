## Problem Set
These are from https://leetcode.com, and are from the "easy" problem set. They're not _that_ easy—there are some subtleties here and there—but they're mainly good practice for writing unit tests, which is why I call them out.
- [Remove Element](https://leetcode.com/problems/remove-element/)
- [Valid Parentheses](https://leetcode.com/problems/valid-parentheses/) — _This one is important_
- [Merge Two Sorted Lists](https://leetcode.com/problems/merge-two-sorted-lists/)
- [strStr](https://leetcode.com/problems/implement-strstr/)
- [Search Insert Position](https://leetcode.com/problems/search-insert-position/)

The main learning objectives here are:
- Identifying edge cases
- Reasoning about indices
