title: Testing
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}
]

---

class: impact

# {{title}}
## Types of Testing

---

# Agenda
- Types of Tests
- Unit Tests by Example
- TDD with `unittest`
- Features by Example
- BDD with `aloe`

---

## Types of Tests
- **Unit**:
- **Integration/End-to-End**:
- **Acceptance**: 

---

### Unit Tests

---

### Integration Tests

---

### Acceptance Tests

---

## Unit Tests by Example

---

### Example 1: Python

---

### Example 2: Go

---

### Example 3: JavaScript

---

## TDD with `unittest`

---

### Directory Structure

---

### Designing Test Cases

---

### Test-Driven Development

???
Also see [nose](https://code.tutsplus.com/tutorials/beginning-test-driven-development-in-python--net-30137).

---

## Features by Example

---

### Writing Features

---

### Writing Steps

---

## FIN

---

