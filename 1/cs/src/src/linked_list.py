from __future__ import annotations
from dataclasses import dataclass
from typing import Any, Callable, Generic, Optional, TypeVar

T = TypeVar('V')


@dataclass
class Node(Generic[T]):
    val: T
    next: Node = None

    @staticmethod
    def forceNode(node: Optional[Node]) -> Node:
        if node is None:
            return Node(None)
        elif isinstance(node, Node):
            return node
        else:
            # else, assume `node` is value
            return Node(node)

    def __eq__(self, other: Node) -> bool:
        return True if self.val == other.val else False


@dataclass
class LinkedList:
    head: Node

    def __init__(self, head: Node = None):
        self.head = head

    def append(self, node: Node, to_head=True) -> Node:
        if to_head:
            head = Node.forceNode(node)

            head.next = self.head
            self.head = head
        else:
            current: Node = self.head
            while current.next:
                if current.next is None:
                    # Reached end of List
                    current.next = Node.forceNode(node)
                    break

                current = current.next

        return self

    def insert_at(self, targetIndex: int, target: T) -> List:
        current: Node = self.head
        parent: Optional[Node] = None
        target: Node = Node.forceNode(target)
        index: int = 0

        while current.next:
            if index == targetIndex:
                if parent is not None:
                    parent.next = target
                    target.next = current
                else:
                    target.next = current
                    self.head = target

                return self

            current = current.next

        return self

    def delete(self, target: T) -> Node:
        current: Node = self.head
        parent: Node = None
        target: Node = Node.forceNode(target)

        while current.next:
            if current.val == target.val:
                if parent is None:
                    self.head = current.next
                else:
                    parent.next = current.next
            break

            parent = current
            current = current.next

        return self

    def delete_at(self, targetIndex: int) -> Node:
        current: Node = self.head
        parent: Node = None
        target: Node = Node.forceNode(target)
        index: int = 0

        while current.next:
            if index == targetIndex:
                if parent is None:
                    self.head = current.next
                else:
                    # Throw away `current`/Delete current item
                    parent.next = current.next
            break

            parent = current
            current = current.next
            index += 1

        return self

    def search(self, target: T) -> Optional[Node]:
        current: Node = self.head
        target: Node = Node.forceNode(target)

        while current.next:
            if current.val == target.val:
                return current

            current = current.next

        return None

    def visit(self, fn: Callable) -> List[Any]:
        results: List[Any] = list()

        while current.next:
            results.append(fn(current))

        return results

