import unittest
from src.linked_list import Node, LinkedList

class TestLinkedList(unittest.TestCase):
    
    def setUp(self):
        pass

    def test_appends_raw_value(self):
        start = LinkedList(Node(1))

        # Order is inverted because we add to front by default
        expected = LinkedList(Node(2, Node(1, None)))
        actual = start.append(2)

        self.assertEqual(expected, actual)

        # Order is inverted because we add to tail w/ `to_head=False`
        expected = LinkedList(Node(1, Node(2, None)))
        actual = start.append(2, to_head=False)

    def test_appends_node(self):
        start = LinkedList(Node(1))

        # Order is inverted because we add to front by default
        expected = LinkedList(Node(2, Node(1, None)))
        actual = start.append(Node(2))

        self.assertEqual(expected, actual)

        # Order is inverted because we add to tail w/ `to_head=False`
        expected = LinkedList(Node(1, Node(2, None)))
        actual = start.append(Node(2), to_head=False)

    def test_delete_raw_value(self):
        start = LinkedList(Node(2, Node(1, None)))

        expected = LinkedList(Node(1, Node(None)))
        actual = start.delete(2)

        self.assertEqual(expected, actual)

    def test_delete_node(self):
        start = LinkedList(Node(2, Node(1, None)))

        expected = LinkedList(Node(1, Node(None)))
        actual = start.delete(Node(2))

        self.assertEqual(expected, actual)

    def test_search_raw_value(self):
        start = LinkedList(Node(2, Node(1, None)))
        

        expected = Node(2)
        actual = start.search(2)

        self.assertEqual(expected, actual)

    def test_search_node(self):
        start = LinkedList(Node(2, Node(1, None)))
        
        expected = None
        actual = start.search(Node(3))

        self.assertEqual(expected, actual)

if __name__ == "__main__":
    unittest.main()
