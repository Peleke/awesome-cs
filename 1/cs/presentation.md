title: Arrays & Lists
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  {{title}}
]

---

class: impact

# {{title}}

---

# Agenda
- Arrays
- Linked Lists
- Operations on Linked Lists

---

---

## Operations on Lists

---

### Length

---

### Search

---

### Append: At Tail

---

### Append: At Head

---

### InsertAt

---

### Delete: From Tail

---

### Delete: From Head

---

### RemoveFrom

---

### Visit

---

### Max

---

### Min

---

## FIN
